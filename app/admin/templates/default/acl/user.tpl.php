<form action="" class="pure-form" method="post">
    <table class="pure-table pure-table-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>用户名</th>
            <th>密码</th>
            <th>状态</th>
            <th>角色</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['u'] as $u) : ?>
            <tr>
                <td width="30"><?php echo $u['id'] ?></td>
                <td><input type="text" name="a[<?php echo $u['id'] ?>][name]" value="<?php echo $u['name'] ?>"
                           id=""/></td>
                <td><input type="text" style="width:365px" name="a[<?php echo $u['id'] ?>][password]"
                           value="<?php echo $u['password'] ?>" id=""/></td>
                <td><input type="text" style="width:35px" name="a[<?php echo $u['id'] ?>][t]"
                           value="<?php echo $u['t'] ?>" id=""/></td>
                <td>
                    <select name="a[<?php echo $u['id'] ?>][rid]" id="">
                        <?php foreach ($data['roles'] as $r) : ?>
                            <?php if ($r['id'] == $u['rid']): ?>
                                <option value="<?php echo $r['id'] ?>" selected><?php echo $r['name'] ?></option>
                            <?php else : ?>
                                <option value="<?php echo $r['id'] ?>"><?php echo $r['name'] ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </td>
                <td>
                    <a href="<?php echo $this->link('acl:delUser', array('uid' => $u['id'])) ?>">删除</a>
                </td>
            </tr>
        <?php endforeach ?>
        <tr>
            <td width="30">+</td>
            <td><input type="text" name="a[+][name]" value="" id=""/></td>
            <td><input type="text" style="width:365px" name="a[+][password]" value="" id=""/></td>
            <td><input type="text" style="width:35px" name="a[+][t]" value="" id=""/></td>
            <td>
                <select name="a[+][rid]" id="">
                    <?php foreach ($data['roles'] as $r) : ?>
                        <option value="<?php echo $r['id'] ?>"><?php echo $r['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </td>
            <td></td>
        </tr>
        </tbody>
    </table>
    <div style="padding-top:10px;text-align:left;">
        <input class="pure-button" type="submit" value="保存"/>
    </div>
</form>
