<div class="pure-u-1">
    <form id="form_nav" class="pure-form" action="" method="post">
        <div style="margin-top: 20px;">
            <table class="pure-table pure-table-horizontal">
                <thead>
                <tr>
                    <th style="text-align: center">ID</th>
                    <th style="text-align: center">菜单名称</th>
                    <th style="text-align: center">类名称</th>
                    <th style="text-align: center">是否显示</th>
                    <th style="text-align: center">排序</th>
                    <th style="text-align: center">操作</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($data["menu"] as $m) : ?>
                    <tr>
                        <td>
                            <?php echo $m['id'] ?>
                            <input type="hidden" id="ele_id" name="id" value=""/>
                            <input type="hidden" name="nav[<?php echo $m['id'] ?>][id]" id=""
                                   value="<?php echo $m['id'] ?>"/>
                        </td>
                        <td><input type="text" name="nav[<?php echo $m['id'] ?>][name]" id=""
                                   value="<?php echo $m['name'] ?>"/></td>
                        <td><input type="text" name="nav[<?php echo $m['id'] ?>][link]" id=""
                                   value="<?php echo $m['link'] ?>"/></td>
                        <td><input type="text" name="nav[<?php echo $m['id'] ?>][status]" id=""
                                   value="<?php echo $m['status'] ?>"/></td>
                        <td><input type="text" name="nav[<?php echo $m['id'] ?>][order]" id=""
                                   value="<?php echo $m['order'] ?>"/></td>
                        <td>
                            <a href="<?php echo $this->link("acl:del", array('id' => $m['id'])) ?>" onclick="return confirm('确实要删除吗?')">删除</a>
                            <a href="<?php echo $this->link("acl:editMenu", array('m' => $m['link'])) ?>">编辑子菜单</a>
                        </td>
                    </tr>
                <?php endforeach ?>

                <?php foreach ($data["un_save_menu"] as $k => $m) : ?>
                    <tr>
                        <td>+</td>
                        <td>
                            <input type="text" name="addNav[<?= $k + 1 ?>][name]" value="<?= $m['name'] ?>">
                        </td>
                        <td>
                            <input type="text" name="addNav[<?= $k + 1 ?>][link]" value="<?= $m['link'] ?>">
                        </td>
                        <td>
                            <input type="text" name="addNav[<?= $k + 1 ?>][status]" id="">
                        </td>
                        <td>
                            <input type="text" name="addNav[<?= $k + 1 ?>][order]" id="">
                        </td>
                        <td>
                        </td>
                    </tr>
                <?php endforeach ?>

                <tr>
                    <td>+</td>
                    <td>
                        <input type="text" name="addNav[0][name]">
                    </td>
                    <td>
                        <input type="text" name="addNav[0][link]" id="">
                    </td>
                    <td>
                        <input type="text" name="addNav[0][status]" id="">
                    </td>
                    <td>
                        <input type="text" name="addNav[0][order]" id="">
                    </td>
                    <td>
                    </td>
                </tr>
                </tbody>
            </table>

            <input type="submit" style="float:left;margin:10px 0;" class="pure-button" name="save" value="保存"/>
        </div>
    </form>
</div>
