<form action="" method="post" class="pure-form">
    <table class="pure-table pure-table-horizontal">
        <tbody>
        <tr>
            <td width="100">名称</td>
            <td align="left">
                <input type="text" name="name" id="" value="<?php echo $data['role_info']['name'] ?>"/>
                <input type="hidden" name="rid" value="<?php echo $data['role_info']['id'] ?>"/>
            </td>
        </tr>

        <tr>
            <td>权限</td>
            <td>
                <?php
                    $this->renderTpl('acl/acl_behavior', array(
                        'menu_list' => $data['menu_list'],
                        'menu_select' =>  $data ['menu_select'],
                    ))
                ?>
            </td>
        </tr>

        <tr>
            <td></td>
            <td><input class="pure-button" type="submit" value="保存"/></td>
        </tr>
        </tbody>
    </table>
</form>
