<div class="jumbotron" style="background: url('<?php echo $this->res('images/congruent_pentagon.png') ?>')">
    <div class="container">
        <h1>Hello World</h1>
        <p>OOP，MVC+，Layer，Annotate，双向智能别名，PSR，Composer</p>
        <p>
            <a href="./../admin" target="_blank">后台管理demo</a>
            <a href="http://www.crossphp.com" target="_blank">框架主页</a>
        </p>
        <p>
            <a class="btn btn-primary btn-lg" href="http://document.crossphp.com" target="_blank" role="button">查看文档 »</a>
        </p>
    </div>
</div>
<div class="container">
    <?php
    if (! empty($data)) {
        printf('Version: %s<br>', $data['version']);
    }
    ?>
</div>
<div class="container">
    <footer>
        <p>© crossphp.com 2014-2015</p>
    </footer>
</div>
